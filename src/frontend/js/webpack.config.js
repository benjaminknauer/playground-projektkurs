const path = require('path');

module.exports = {
  entry: { index: './src/index.js', chat: './src/chat.js'},
  output: {
    filename: '[name]_main.js',
    path: path.resolve(__dirname, '../static/js'),
  },
  mode: 'development',
  watch: false
};