import Cookies from "js-cookie";


// main

const nickname = Cookies.get("nickname");
if (nickname == undefined) {
    window.location.replace("/")
}

let chat_counter = 0;
let chat = [];
let msg_box = document.getElementById("msg_box");

update();


// Send
document.getElementById("send_button").addEventListener("click", send_msg);
document.addEventListener('keydown', function (event) {
    if (event.keyCode == 13) {
        send_msg();
    }
});

async function send_msg() {

    const msg_txt = await document.getElementById("msg").value.trim();
    if (msg_txt != "") {

        if (nickname == null) {
            window.location.replace("/");

        } else {

            const content = {
                "user": {"nickname": nickname, "msg": msg_txt},
                "tech": {"get_chat_from": chat_counter}
            };

            document.getElementById("msg").value = "";


            const server_resp = await fetch(`/send_msg`, {
                method: "POST",
                body: JSON.stringify(content),
                headers: new Headers({
                    "content-type": "application/json"
                })
            });

            if ((await server_resp.json()["msg_saved"])) {
                draw_msg(await server_resp.json()["msg_saved"], "Comand was executed!")
            }

            update();

        }
    }
}


document.getElementById("update_button").addEventListener("click", update);

async function update() {
    const chat_hist = await (await fetch(`/update`, {
        method: "POST",
        headers: new Headers({
            "content-type": "application/json"
        })
    })).json();


    // Displaystuff
    msg_box.innerHTML = draw_msg("System", "Just text  a massage", 0);
    const l = chat_hist["chat_len"];
    for (let i = 0; i < l; i++) {
        try {
            let m = chat_hist["chat_hist"][i];

            msg_box.innerHTML += draw_msg(m["nickname"], m["msg"], m["time"]);


        } catch {
        }
    }
    console.log(chat);

}

function draw_msg(nick, txt, time) {

    const t = new Date(time) // not working
    const msg_layout = `<div id="chat_msg"><table><tr>
            <td id="chat_nick_name"><p>${nick}</p></td>
            <td id="msg_time"><p>[${t.getHours()}:${t.getMinutes()}]</p></td></tr></table>
            <div id="chat_msg_content"><p>${txt}</p></div>
            </div>`;
    return msg_layout;
}

