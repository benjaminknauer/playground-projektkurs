from time import time
from typing import Optional
from pythons.selfmadehelpers import JSONableList


__history__ = JSONableList()



"""
sample_msg = {"nickname": "x",
              "msg_cont": "hallo",
              "time": 1651399001.468739 
              "ip": "127.0.0.1"}
"""


def get_history() -> Optional[JSONableList]:
    global __history__
    if __history__:
        return __history__
    else:
        return None


def add_msg(msg: dict, ip_addrs=None):
    global __history__
    msg.update(time=time())
    m = msg["msg"]
    if bool(m.count("%")):      # for potential more cmds
        if m.lower().count("%c"):
            __history__.clear()
            __history__.append({
                "nickname": "System",
                "msg": f"'{msg['nickname']}' hast cleared the chat",
                "time": time()})
        return False

    elif ip_addrs and isinstance(ip_addrs, str):
        msg.update(ip=ip_addrs)

    __history__.append(msg)
    return True


def __t():
    g = {"nickname": "x", "msg_cont": "hallo"}
    h = {"nickname": "z", "msg_cont": "hi"}
    add_msg(g, "192.168.0.45")
    from time import sleep
    sleep(1)
    add_msg(h)
    print(get_history())

#__t()
