from os import path
from flask import Flask, render_template, request
from pythons.chatstuff import add_msg, get_history

template_dir = path.abspath("frontend/templates")
app = Flask(__name__, static_url_path='/', static_folder='frontend/static', template_folder=template_dir)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/chat")
def chat():
    return render_template("chat.html")

@app.route("/update", methods=["POST"])
def update():
    hist = get_history()
    if hist is None:
        l = 0
    else:
        l = len(hist)
    return {"chat_hist": hist, "chat_len": l}, 202

@app.route("/send_msg", methods=["POST"])
def send():

    r = request.get_json()
    i = add_msg(r["user"])
    print(r)

    return {"msg_saved": i}, 202


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)