FROM python:3-alpine

COPY src /app
COPY requirements.txt /app/

RUN apk add --update nodejs npm
RUN cd /app/frontend/js && npm ci && npm run build

WORKDIR /app
RUN pip install -r requirements.txt

EXPOSE 80
ENTRYPOINT ["python", "server.py"]